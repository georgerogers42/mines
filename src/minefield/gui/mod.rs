use std::rc::Rc;
use std::cell::RefCell;

use minefield::Minefield;

use gtk;
use gtk::traits::*;
use gtk::WindowType::*;
use gtk::Orientation::*;

fn score_line(m: &Minefield) -> String {
    format!(
        "{{Lives: {}, Mines: {}}}",
        m.lives,
        m.mine_count(),
    )
}

fn result_line(m: &Minefield) -> String {
    if m.lives > 0 {
        format!("You Are Winner: {}", score_line(m))
    } else {
        format!("You Are Loser: {}", score_line(m))
    }
}

pub fn run() {
    gtk::init().unwrap();
    let w = gtk::Window::new(Toplevel);
    let b = gtk::Box::new(Vertical, 10);
    let wl = gtk::Label::new(Some("Width"));
    b.add(&wl);
    let wid = gtk::Entry::new();
    wid.set_text("10");
    b.add(&wid);
    let hl = gtk::Label::new(Some("Height"));
    b.add(&hl);
    let het = gtk::Entry::new();
    b.add(&het);
    het.set_text("10");
    let st = gtk::Button::new_with_label("Start");
    {
        let wid = wid.clone();
        let het = het.clone();
        st.connect_clicked(move |_| {
            let gw = game_window(
                wid.get_text().unwrap_or("".to_owned()).parse().unwrap_or(
                    10,
                ),
                het.get_text().unwrap_or("".to_owned()).parse().unwrap_or(
                    10,
                ),
            );
            gw.show_all();
        });
    }
    b.add(&st);
    w.add(&b);
    w.connect_delete_event(move |_, _| {
        gtk::main_quit();
        gtk::Inhibit(false)
    });
    w.show_all();
    gtk::main();
}

fn game_window(w: usize, h: usize) -> gtk::Window {
    let f = gtk::Window::new(Toplevel);
    let gb = gtk::Box::new(Vertical, 20);
    let mf = Rc::new(RefCell::new(Minefield::new(w, h, 10, 10)));
    let gr = gtk::Label::new(Some(&score_line(&mf.borrow()) as &str));
    gb.add(&gr);
    let vb = gtk::Box::new(Vertical, 10);
    for i in 0..10 {
        let hb = gtk::Box::new(Horizontal, 10);
        for j in 0..10 {
            let b = gtk::Button::new_with_label(" ");
            let gr = gr.clone();
            let mf = mf.clone();
            b.connect_clicked(move |b| {
                let mut m = mf.borrow_mut();
                if m.done() {
                    return;
                }
                match m.click(i, j) {
                    Some(s) => {
                        b.set_label(&s.to_string());
                    }
                    None => {
                        b.set_label("*");
                        gr.set_label(&score_line(&m));
                    }
                }
                if m.done() {
                    gr.set_label(&result_line(&m));
                }
            });
            hb.add(&b);
        }
        vb.add(&hb)
    }
    gb.add(&vb);
    f.add(&gb);
    f
}
