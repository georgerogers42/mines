use rand;
use rand::Rng;
pub mod gui;

#[derive(PartialEq, Eq, Hash, Clone, Debug)]
pub struct Minefield {
    pub lives: usize,
    pub mines: Vec<Vec<(bool, bool)>>,
}

impl Minefield {
    pub fn new(len: usize, wid: usize, chalenge: u32, lives: usize) -> Minefield {
        let mut rng = rand::thread_rng();
        let mines = (0..len)
            .map(|_| {
                (0..wid)
                    .map(|_| (!rng.gen_weighted_bool(chalenge), false))
                    .collect()
            })
            .collect();
        Minefield {
            lives,
            mines: mines,
        }
    }
    pub fn mine_count(&self) -> usize {
        self.mines.iter().flat_map(|x| x).filter(|x| !x.0).count()
    }
    pub fn len(&self) -> usize {
        self.mines.len()
    }
    pub fn wid(&self) -> usize {
        self.mines[0].len()
    }
    pub fn suroundings(&self, i: usize, j: usize) -> Vec<(usize, usize)> {
        let is = i as isize - 1..i as isize + 2;
        let ps = is.flat_map(|x| {
            let js = j as isize - 1..j as isize + 2;
            js.map(|y| (x, y)).collect::<Vec<(isize, isize)>>()
        });
        ps.filter(|&(x, y)| {
            x >= 0 && y >= 0 && x < self.len() as isize && y < self.wid() as isize
        }).map(|(x, y)| (x as usize, y as usize))
            .collect()
    }
    pub fn safe(&self, i: usize, j: usize) -> Option<usize> {
        if self.mines[i][j].0 {
            Some(
                self.suroundings(i, j)
                    .into_iter()
                    .map(|(x, y)| self.mines[x][y].0 as usize)
                    .sum::<usize>(),
            )
        } else {
            None
        }
    }
    pub fn click(&mut self, i: usize, j: usize) -> Option<usize> {
        let s = self.safe(i, j);
        if s.is_none() && !self.mines[i][j].1 {
            self.lives -= 1;
        }
        self.mines[i][j].1 = true;
        s
    }
    pub fn done(&self) -> bool {
        let mut r = true;
        for i in 0..self.len() {
            for j in 0..self.wid() {
                r = r && (self.mines[i][j].1 || !self.mines[i][j].0);
            }
        }
        r
    }
}
