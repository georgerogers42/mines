extern crate gtk;
extern crate rand;

use minefield::gui::run;

pub mod minefield;

fn main() {
    run();
}
